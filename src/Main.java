public class Main {
    public static void main(String[] args) {
        Pila pila = new Pila();
        pila.push(1);
        pila.push(2);
        pila.push(3);
        pila.push(12);

        System.out.println("Pila Inicial: " + pila);

        System.out.println("Size: " + pila.size());
        System.out.println("Esta Vacia?: " + pila.isEmpty());
        System.out.println("Tope o cima de la Pila: " + pila.peek());


        System.out.println("Estos son los elementos de la pila:");
        while (!pila.isEmpty()){
            System.out.println(pila.pop());
        }


    }
}