public class Pila {

    private Nodo cima;

    public Pila(){
        this.cima = null;
    }

    public void push(int dato){
        Nodo nuevoNodo =  new Nodo(dato);
        nuevoNodo.siguiente = cima;
        cima =  nuevoNodo;
    }

    public boolean isEmpty(){
        return cima == null;
    }

    public int pop(){

        if(isEmpty()){
            throw new IllegalStateException("La pila esta Vacia");
        }

        int dato = cima.dato;
        cima = cima.siguiente;
        return dato;

    }

    public int peek(){
        if(isEmpty()){
            throw new IllegalStateException("La pila esta Vacia");
        }

        return cima.dato;
    }

    public int size(){
        int sizePila = 0;
        Nodo actual = cima;
        while (actual != null){
            sizePila++;
            actual = actual.siguiente;
        }
        return sizePila;
    }


}
